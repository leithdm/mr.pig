import SceneKit
import Foundation

extension SCNAction {
  
  class func waitForDurationThenRemoveFromParent(duration:NSTimeInterval) -> SCNAction {
    let wait = SCNAction.waitForDuration(duration)
    let remove = SCNAction.removeFromParentNode()
    return SCNAction.sequence([wait,remove])
  }
  
  class func waitForDurationThenRunBlock(duration:NSTimeInterval, block: ((SCNNode!) -> Void) ) -> SCNAction {
    let wait = SCNAction.waitForDuration(duration)
    let runBlock = SCNAction.runBlock { (node) -> Void in
      block(node)
    }
    return SCNAction.sequence([wait,runBlock])
  }
  
  class func rotateByXForever(x:CGFloat, y:CGFloat, z:CGFloat, duration:NSTimeInterval) -> SCNAction {
    let rotate = SCNAction.rotateByX(x, y: y, z: z, duration: duration)
    return SCNAction.repeatActionForever(rotate)
  }
  
}