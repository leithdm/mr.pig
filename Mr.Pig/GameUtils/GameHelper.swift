import Foundation
import SceneKit
import SpriteKit

public enum GameStateType {
  case Playing
  case TapToPlay
  case GameOver
}

class GameHelper {
  
  var coinsBanked:Int
  var coinsCollected:Int
  var state = GameStateType.TapToPlay
  
  var hudNode:SCNNode!
  var labelNode:SKLabelNode!
  
  
  static let sharedInstance = GameHelper()
  
  var sounds:[String:SCNAudioSource] = [:]
  
  private init() {
    coinsCollected = 0
    coinsBanked = 0
    initHUD()
  }
  
  func initHUD() {
    
    let skScene = SKScene(size: CGSize(width: 500, height: 100))
    skScene.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
    
    labelNode = SKLabelNode(fontNamed: "Menlo-Bold")
    labelNode.fontSize = 20
    labelNode.position.y = 50
    labelNode.position.x = 250
    
    skScene.addChild(labelNode)
    
    let plane = SCNPlane(width: 5, height: 1)
    let material = SCNMaterial()
    material.lightingModelName = SCNLightingModelConstant
    material.doubleSided = true
    material.diffuse.contents = skScene
    plane.materials = [material]
    
    hudNode = SCNNode(geometry: plane)
    hudNode.name = "HUD"
    hudNode.rotation = SCNVector4(x: 1, y: 0, z: 0, w: 3.14159265)
    hudNode.position = SCNVector3(x:0, y: 1.8, z: -5)
  }
  
  func updateHUD() {
    let coinsBankedFormatted = String(format: "%0\(4)d", coinsBanked)
    let coinsCollectedFormatted = String(format: "%0\(4)d", coinsCollected)
    labelNode.text = "🐽\(coinsCollectedFormatted) | 🏡\(coinsBankedFormatted)"
  }
  
  func loadSound(name:String, fileNamed:String) {
    if let sound = SCNAudioSource(fileNamed: fileNamed) {
      sound.positional = false
      sound.volume = 0.3
      sound.load()
      sounds[name] = sound
    }
  }
  
  func playSound(node:SCNNode, name:String) {
    let sound = sounds[name]
    node.runAction(SCNAction.playAudioSource(sound!, waitForCompletion: false))
  }
  
  func collectCoin() {
    coinsCollected += 1
  }
  
  func bankCoins() -> Bool {
    coinsBanked += coinsCollected
    
    if coinsCollected > 0 {
      coinsCollected = 0
      return true
    }
    
    return false
  }
  
  func reset() {
    coinsCollected = 0
    coinsBanked = 0
  }
}